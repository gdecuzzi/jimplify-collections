package org.uqbar.jimplify.protocol;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class SimpleCollectionAllSatisfyTest {

	@Test
	public void testAllSatisfyInEmptyCollectionIsTrue() {
		EasyCollection<Integer> collection = SimpleCollection.of(new ArrayList<Integer>());
		assertThat(collection.allSatisfy((number) -> number > 0)).isTrue();
	}

	@Test
	public void testAllSatisfyWhenAllSatisfiesConditionYieldsTrue() {
		EasyCollection<Integer> collection = SimpleCollection.of(Arrays.asList(2, 4, 6, 4, 8, 10, 24));
		assertThat(collection.allSatisfy((number) -> number % 2 == 0)).isTrue();
	}

	@Test
	public void testAllSatisfyWhenAnyElementDontSatisfiesTheConditionYieldsFalse() {
		EasyCollection<Integer> collection = SimpleCollection.of(Arrays.asList(2, 3, 6, 4, 8, 10, 24));
		assertThat(collection.allSatisfy((number) -> number % 2 == 0)).isFalse();
	}

}
