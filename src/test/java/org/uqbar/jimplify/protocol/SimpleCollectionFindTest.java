package org.uqbar.jimplify.protocol;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.uqbar.jimplify.errors.ObjectNotFoundException;

public class SimpleCollectionFindTest {


	@Test
	public void testFindElementInEmptyCollectionRaisesAnException() {
		EasyCollection<Number> collection = SimpleCollection.of(new ArrayList<Number>());
		Number found = null;
		try {
			found = collection.find((number) -> number.intValue() > 0);
		} catch (ObjectNotFoundException e) {
			assertThat(found).isNull();
			assertThat(e.getMessage()).isEqualTo("Not element found to satisfy the condition");
		}
	}
	
	@Test
	public void testFindFindsACorrectValue() {
		EasyCollection<String> collection = SimpleCollection.of(Arrays.asList("aaa","b","cc","ddd","e"));
		
		String found = collection.find((string)-> string.length() == 3);

		assertThat(found).isNotNull();
		assertThat(found).isIn(Arrays.asList("aaa","ddd"));
	}
	
	@Test
	public void testFindWhenNoOneSatisfyRaisesError() {
		List<Integer> numbers = Arrays.asList(10,20,30,40);
		
		EasyCollection<Integer> collection = SimpleCollection.of(numbers);
		Integer found = null;
		try {
			found = collection.find((num)-> num * 10 > 1000);
		} catch (ObjectNotFoundException e) {
			assertThat(found).isNull();
			assertThat(e.getMessage()).isEqualTo("Not element found to satisfy the condition");
		}
	}


}
