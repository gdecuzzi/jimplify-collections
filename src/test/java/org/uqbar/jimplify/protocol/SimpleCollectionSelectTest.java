package org.uqbar.jimplify.protocol;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class SimpleCollectionSelectTest {

	@Test
	public void testSelectElementsInEmptyCollectionIsEmpty() {
		EasyCollection<Number> collection = SimpleCollection.of(new ArrayList<Number>());
		assertThat(collection.select((number) -> number.intValue() > 0)).isEmpty();
	}
	
	@Test
	public void testSelectContainsOnlyTheCorrectOnes() {
		EasyCollection<String> collection = SimpleCollection.of(Arrays.asList("aaa","b","cc","ddd","e"));
		assertThat(collection.select((string)-> string.length() == 3)).containsExactly("aaa","ddd");
	}
	
	@Test
	public void testSelectWhenNoOneSatisfyIsEmpty() {
		EasyCollection<Integer> collection = SimpleCollection.of(Arrays.asList(10,20,30,40));
		assertThat(collection.select((num)-> num * 10 > 1000)).isEmpty();
	}

}
