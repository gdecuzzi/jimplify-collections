package org.uqbar.jimplify.protocol;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class SimpleCollectionCollectTest {

	@Test
	public void testCollectElementsInEmptyCollectionIsEmpty() {
		EasyCollection<Integer> collection = SimpleCollection.of(new ArrayList<Integer>());
		assertThat(collection.collect((number) -> number + 2)).isEmpty();
	}
	
	@Test
	public void testCollectTransformNumbers() {
		EasyCollection<Integer> collection = SimpleCollection.of(Arrays.asList(10,20,30,40));
		assertThat(collection.collect((num)-> num * 10)).containsExactly(100,200,300,400);
	}
	
	@Test
	public void testCollectFromSetNotReturnDuplicates() {
		Set<Integer> numbers = new HashSet<Integer>(Arrays.asList(10,-10));
		EasyCollection<Integer> collection = SimpleCollection.of(numbers);
		Collection<Integer> transformed = collection.collect((num)-> Math.abs(num));
		assertThat(transformed).containsExactly(10);
	}

}
