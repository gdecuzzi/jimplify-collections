package org.uqbar.jimplify.protocol;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class SimpleCollectionAnySatisfyTest {

	@Test
	public void testAnySatisfyInEmptyIsFalse() {
		EasyCollection<Integer> collection = SimpleCollection.of(new ArrayList<Integer>());
		assertThat(collection.anySatisfy((number) -> number > 0)).isFalse();
	}
	
	@Test
	public void testAnySatisfyWhenSatisfiesIsTrue() {
		EasyCollection<String> collection = SimpleCollection.of(Arrays.asList("aaa","b","cc","ddd","e"));
		Boolean satisfy = collection.anySatisfy((string)-> string.length() == 3);
		assertThat(satisfy).isTrue();
	}
	
	@Test
	public void testAnySatisfyWhenNoneSatisfiesIsFalse() {
		EasyCollection<String> collection = SimpleCollection.of(Arrays.asList("aaa","b","cc","ddd","e"));
		Boolean satisfy = collection.anySatisfy((string)-> string.length() > 30);
		assertThat(satisfy).isFalse();
	}

}
