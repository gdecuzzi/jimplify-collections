package org.uqbar.jimplify.protocol;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class SimpleCollectionInjectIntoTest {

	@Test
	public void testInjectIntoAsSumWorksCorrectly() {
		EasyCollection<Integer> collection =SimpleCollection.of(Arrays.asList(10, 20, 30, 40));
		assertThat(collection.injectInto(0, (seed, num) -> num + seed)).isEqualTo(100);
	}

	@Test
	public void testInjectIntoAsCollectWorksCorrectly() {
		EasyCollection<String> collection = SimpleCollection.of(Arrays.asList("aaa", "b", "cc", "ddd", "e"));
		assertThat(collection.injectInto(
				new ArrayList<Integer>(), 
				(seed, string) -> {	seed.add(string.length()); return seed;	} )
		).isEqualTo(Arrays.asList(3, 1, 2, 3, 1));
	}

	@Test
	public void testInjectIntoAsMaxWorksCorrectly() {
		List<Integer> numbers = Arrays.asList(10, 20, 30, 40);
		EasyCollection<Integer> collection = SimpleCollection.of(numbers);
		assertThat(collection.injectInto(numbers.get(0), (seed, num) -> seed > num ? seed : num)).isEqualTo(40);
	}

}
