package org.uqbar.jimplify.errors;

/**
 * Raise an specific exception when we ask for something expecting a value as result
 * and we get a null instead. 
 * @author Gi
 *
 */
public class ObjectNotFoundException extends RuntimeException {

	public ObjectNotFoundException(String message) {
		super(message);
	}


}
