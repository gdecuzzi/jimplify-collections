package org.uqbar.jimplify.errors;

import java.util.Collection;

@SuppressWarnings("rawtypes")
public class CannotInstantiateCollection extends RuntimeException {

	private Collection problematicCollection;

	public CannotInstantiateCollection(Collection realCollection, Throwable rootCause) {
		super("Cannot obtain a new instance of: " + realCollection, rootCause);
		problematicCollection = realCollection;
	}

	protected Collection getProblematicCollection() {
		return problematicCollection;
	}

	protected void setProblematicCollection(Collection problematicCollection) {
		this.problematicCollection = problematicCollection;
	}
	

}
