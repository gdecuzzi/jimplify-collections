package org.uqbar.jimplify.protocol;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * This Class defines the wanted protocol for collections
 * This is NOT lazy and the idea is only to reduce burocracy
 * on the actual state of collections on Java 8.
 * 
 * This is based on smalltalk's collection style
 * @author Gi
 *
 */
public interface EasyCollection <T> {
	
	/**
	 * Filters the collection with the given predicate.
	 * @param condition
	 * 			A lambda that expects one element of the collection and returns a boolean value.
	 * @return
	 * 			A new collection with the elements matching the given condition.
	 */
	public Collection<T> select(Predicate<T> condition);
	
	/**
	 * Returns a new Collection with the elements transformed.
	 * @param transformation
	 * 			A transformation function. Expects an element of the same type than the original collection element and yields something of other type.
	 * @return 
	 * 			A new collection with the elements transformed using the given transformation.
	 */
	public <R> Collection<R> collect(Function<T, R> transformation);
	
	/**
	 * 
	 * @param condition
	 * @return true if exist at least one element matching the given condition; and false otherwise.
	 */
	public Boolean anySatisfy(Predicate<T> condition);
	
	/**
	 * 
	 * @param condition
	 * @return @return true if all the elements in the collection match the given condition; and false otherwise.
	 */
	public Boolean allSatisfy(Predicate<T> condition);
	
	/**
	 * finds an element matching the given condition. If no element matches it, it throws an exception.
	 * 
	 * @param condition the predicate to meet.
	 * @return the element
	 * @throws ObjectNotFoundException when no object meets the given condition.
	 */
	public T find(Predicate<T> condition);
	
	/**
	 * Performs a reduction
	 * @param initValue
	 * 			The "seed" of the operation to realize reduction with.
	 * @param operation
	 * 			the operation that yields the seed for the next iteration. 
	 * 			The return type must be the same as the seed type. 
	 * @return
	 * 			The reduction of applying the operation to the whole collection.
	 * 			The return type is the same than the Init value type and the return value of the operation.
	 */
	public <R> R injectInto(R initValue, BiFunction<R, T, R> operation);

}

