# README #

To start using add as a dependency:

```
#!xml

<dependency>
  <groupId>org.uqbar.java</groupId>
  <artifactId>JimplifyCollection</artifactId>
  <version>1.0.0</version>
<dependency>
```


### Why does this project exist? ###

The idea is to simplify the usage of collections in java 8.
This is just a wrapper inspired in a collection API that we consideer is a better way

### Example of usage ###


```
#!java

Collection<String> normalCollection = Arrays.asList("aaa","b","cc","ddd","e");
SimpleCollection.of(normalCollection).select((string)-> string.length() == 3);
```